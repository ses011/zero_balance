CREATE TABLE IF NOT EXISTS users (
    uid serial PRIMARY KEY,
    initials text NOT NULL
);

CREATE TABLE IF NOT EXISTS balances (
    uid integer REFERENCES users ON DELETE CASCADE UNIQUE,
    balance money NOT NULL
);

CREATE TABLE IF NOT EXISTS operations (
    oid serial PRIMARY KEY,
    uid integer REFERENCES users ON DELETE CASCADE,
    operation_date TIMESTAMP DEFAULT current_timestamp,
    operation_type VARCHAR(8) CHECK (operation_type = 'accrual' OR  operation_type = 'debiting'),
    amount money NOT NULL,
    current_balance money NOT NULL,
    comment text NOT NULL
);

CREATE INDEX IF NOT EXISTS balances_uid_ind ON balances(uid);

INSERT INTO users(initials) SELECT 'Egor' WHERE NOT EXISTS (SELECT * FROM users);