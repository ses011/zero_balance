module zeroBalance

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/Masterminds/squirrel v1.5.0
	github.com/gin-gonic/gin v1.7.7 // indirect
	github.com/lib/pq v1.10.2
	github.com/stretchr/testify v1.4.0
)
