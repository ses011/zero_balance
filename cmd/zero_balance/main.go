package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"zeroBalance/internal/api_methods"
	"zeroBalance/internal/db"
)

func main() {
	r := gin.Default()

	GetBalance := api_methods.WrapCall(db.Db, api_methods.GetBalance)
	r.GET("/get_balance", GetBalance)

	History := api_methods.WrapCall(db.Db, api_methods.History)
	r.GET("/history", History)

	ChangeBalance := api_methods.WrapCall(db.Db, api_methods.ChangeBalance)
	r.POST("/change_balance", ChangeBalance)

	Transfer := api_methods.WrapCall(db.Db, api_methods.Transfer)
	r.POST("/transfer", Transfer)

	CreateUser := api_methods.WrapCall(db.Db, api_methods.CreateUser)
	r.POST("/create_user", CreateUser)

	log.Fatal(r.Run(":8080"))
}
