package db

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
	"zeroBalance/internal/consts"
)

func TestDateToString(t *testing.T) {
	t.Run("date to string", func(t *testing.T) {
		assert.IsType(t, "string", dateToString(time.Now()))
	})
}

func TestExtractHistory(t *testing.T) {
	columns := []string{"operation_date", "operation_type", "amount", "current_balance", "comment"}

	t.Run("extract all history", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).
				AddRow(time.Now(), "accrual", 123.32, 1233.21, "fff").
				AddRow(time.Now(), "debiting", 123.32, 1233.21, "fff")).
			RowsWillBeClosed()

		req := HistoryRequest{
			Id:       1,
			Limit:    3,
			Offset:   0,
			Type:     "all",
			Order:    "asc",
			SortType: "operation_date",
		}

		var resp []HistoryFrame

		res := ExtractHistory(_db, &resp, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, res)
		assert.Equal(t, 2, len(resp))

		assert.Equal(t, "accrual", resp[0].Operation)
		assert.Equal(t, 123.32, resp[0].Amount)
		assert.Equal(t, 1233.21, resp[0].Balance)
		assert.Equal(t, "fff", resp[0].Comment)

		assert.Equal(t, "debiting", resp[1].Operation)
		assert.Equal(t, 123.32, resp[1].Amount)
		assert.Equal(t, 1233.21, resp[1].Balance)
		assert.Equal(t, "fff", resp[1].Comment)
	})

	t.Run("extract accrual", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).
				AddRow(time.Now(), "accrual", 123.32, 1233.21, "fff").
				AddRow(time.Now(), "accrual", 123.32, 1233.21, "fff")).
			RowsWillBeClosed()

		req := HistoryRequest{
			Id:       1,
			Limit:    3,
			Offset:   0,
			Type:     "accrual",
			Order:    "asc",
			SortType: "operation_date",
		}

		var resp []HistoryFrame

		res := ExtractHistory(_db, &resp, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, res)
		assert.Equal(t, 2, len(resp))

		assert.Equal(t, "accrual", resp[0].Operation)
		assert.Equal(t, 123.32, resp[0].Amount)
		assert.Equal(t, 1233.21, resp[0].Balance)
		assert.Equal(t, "fff", resp[0].Comment)

		assert.Equal(t, "accrual", resp[1].Operation)
		assert.Equal(t, 123.32, resp[1].Amount)
		assert.Equal(t, 1233.21, resp[1].Balance)
		assert.Equal(t, "fff", resp[1].Comment)
	})

	t.Run("extract all history", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).
				AddRow(time.Now(), "debiting", 123.32, 1233.21, "fff").
				AddRow(time.Now(), "debiting", 123.32, 1233.21, "fff")).
			RowsWillBeClosed()

		req := HistoryRequest{
			Id:       1,
			Limit:    3,
			Offset:   0,
			Type:     "debiting",
			Order:    "asc",
			SortType: "operation_date",
		}

		var resp []HistoryFrame

		res := ExtractHistory(_db, &resp, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, res)
		assert.Equal(t, 2, len(resp))

		assert.Equal(t, "debiting", resp[0].Operation)
		assert.Equal(t, 123.32, resp[0].Amount)
		assert.Equal(t, 1233.21, resp[0].Balance)
		assert.Equal(t, "fff", resp[0].Comment)

		assert.Equal(t, "debiting", resp[1].Operation)
		assert.Equal(t, 123.32, resp[1].Amount)
		assert.Equal(t, 1233.21, resp[1].Balance)
		assert.Equal(t, "fff", resp[1].Comment)
	})
}
