package db

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"os"
)

var (
	Db *sql.DB
)

var (
	host     = "database"
	port     = 5432
	user     = os.Getenv("POSTGRES_USER")
	password = os.Getenv("POSTGRES_PASSWORD")
	dbname   = os.Getenv("POSTGRES_DB")
)

func init() {
	if len(os.Args) > 1 && os.Args[1][:5] == "-test" {
		return
	}

	psqlInfo := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		user, password, host, port, dbname)
	db1, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	Db = db1
}
