package db

import (
	"bytes"
	"database/sql"
	sq "github.com/Masterminds/squirrel"
	"text/template"
	"zeroBalance/internal/consts"
)

type ChangeBalanceInfo struct {
	Uid       int
	Op        string
	Operation string
	Amount    float64
	Comment   string
}

type TransferInfo struct {
	From         int
	InitialsFrom string
	To           int
	InitialsTo   string
	Amount       float64
}

func UserExists(_db *sql.DB, id int) bool {
	res := 0
	s := sq.Select("count(*)").From("users").Where(sq.Eq{"uid": id}).PlaceholderFormat(sq.Dollar)
	if err := s.RunWith(_db).QueryRow().Scan(&res); err != nil {
		res = 0
	}
	if res == 0 {
		return false
	}
	return true
}

func GetBalance(_db *sql.DB, id int) (float64, error) {
	var res float64
	s := sq.Select("balance").From("balances").Where(sq.Eq{"uid": id}).PlaceholderFormat(sq.Dollar)
	if err := s.RunWith(_db).QueryRow().Scan(&res); err != nil {
		return -1, err
	}
	return res, nil
}

func createBalance(_db *sql.DB, id int) error {
	i := sq.Insert("balances").Columns("uid", "balance").Values(id, 0.0)
	if _, err := i.RunWith(_db).PlaceholderFormat(sq.Dollar).Exec(); err != nil {
		return err
	}
	return nil
}

func handleAccrual(_db *sql.DB, id int, amount float64) int {
	info := ChangeBalanceInfo{id, "+", "accrual", amount, consts.Accrual}

	tmpl, _ := template.New("sqrReq").Parse(consts.ChangeBalanceTmpl)
	var query bytes.Buffer
	tmpl.Execute(&query, info)

	_db.Exec(query.String())
	return consts.OK
}

func handleDebiting(_db *sql.DB, id int, amount, balance float64) int {
	if balance < amount {
		return consts.SmallBalance
	}

	info := ChangeBalanceInfo{id, "-", "debiting", amount, consts.Debiting}

	var query bytes.Buffer
	tmpl, _ := template.New("sqrReq").Parse(consts.ChangeBalanceTmpl)
	tmpl.Execute(&query, info)
	_db.Exec(query.String())

	return consts.OK
}

func ChangeUserBalance(_db *sql.DB, id int, operation string, amount float64) (float64, int) {
	balance, err := GetBalance(_db, id)

	if err != nil {
		createBalance(_db, id)
		balance = 0
	}

	if operation == "accrual" {
		code := handleAccrual(_db, id, amount)
		return balance + amount, code
	} else {
		code := handleDebiting(_db, id, amount, balance)
		return balance - amount, code
	}
}

func GetInitialsById(_db *sql.DB, id int) string {
	var res string
	s := sq.Select("initials").From("users").Where(sq.Eq{"uid": id}).PlaceholderFormat(sq.Dollar)
	if err := s.RunWith(_db).QueryRow().Scan(&res); err != nil {
		return ""
	}
	return res
}

func Transfer(_db *sql.DB, from, to int, amount float64) int {
	_, err := GetBalance(_db, to)

	if err != nil {
		createBalance(_db, to)
	}

	info := TransferInfo{From: from, To: to, Amount: amount, InitialsFrom: GetInitialsById(_db, from),
		InitialsTo: GetInitialsById(_db, to)}

	var query bytes.Buffer
	tmpl, _ := template.New("sqrReq").Parse(consts.TransferTmpl)
	tmpl.Execute(&query, info)
	_db.Exec(query.String())

	return consts.OK
}

func CreateUser(_db *sql.DB, initials string) (int, int) {
	var uid int

	if len(initials) < 3 {
		return 0, consts.ShortInitials
	}

	i := sq.Insert("users").Columns("initials").
		Values(initials).Suffix("returning uid").RunWith(_db).PlaceholderFormat(sq.Dollar)

	if err := i.QueryRow().Scan(&uid); err != nil {
		return 0, consts.DatabaseError
	} else {
		return uid, consts.OK
	}
}
