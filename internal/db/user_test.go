package db

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
	"zeroBalance/internal/consts"
)

func TestUserExists(t *testing.T) {
	t.Run("user exists", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		res := UserExists(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.True(t, res)
	})

	t.Run("user isnt exists", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(0)).RowsWillBeClosed()

		res := UserExists(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.False(t, res)
	})

	t.Run("user isnt exists (2)", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT")

		res := UserExists(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.False(t, res)
	})
}

func TestGetBalance(t *testing.T) {
	t.Run("get balance OK", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"balance"}).
			AddRow(123.40)).RowsWillBeClosed()

		res, err := GetBalance(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, 123.40, res)
	})

	t.Run("get balance no information", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT")

		res, err := GetBalance(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, -1.0, res)
	})
}

func TestCreateBalance(t *testing.T) {
	t.Run("create balance OK", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectExec("INSERT INTO balances").WithArgs(1, 0.0).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := createBalance(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
	})

	t.Run("create balance already exists", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectExec("INSERT INTO balances").WithArgs(1, 0.0)

		err := createBalance(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
	})
}

func TestGetInitialsById(t *testing.T) {
	t.Run("user exists", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"initials"}).
			AddRow("Egor")).RowsWillBeClosed()

		res := GetInitialsById(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, res, "Egor")
	})

	t.Run("user not exists", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT")

		res := GetInitialsById(_db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, res, "")
	})
}

func TestCreateUser(t *testing.T) {

	t.Run("create new user", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		q := `INSERT INTO users (.+) returning uid`

		mock.ExpectQuery(q).
			WithArgs("foo").
			WillReturnRows(sqlmock.NewRows([]string{"uid"}).AddRow(1)).
			RowsWillBeClosed()

		res, code := CreateUser(_db, "foo")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, 1, res)
		assert.Equal(t, consts.OK, code)
	})

	t.Run("cant create new user", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		q := `INSERT INTO users (.+) returning uid`

		mock.ExpectQuery(q)

		res, code := CreateUser(_db, "foo")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, 0, res)
		assert.Equal(t, consts.DatabaseError, code)
	})
}
