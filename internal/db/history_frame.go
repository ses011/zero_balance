package db

import (
	"bytes"
	"database/sql"
	"text/template"
	"time"
	"zeroBalance/internal/consts"
)

type HistoryFrame struct {
	Date       time.Time `json:"-"`
	DateString string    `json:"date"`
	Operation  string    `json:"operation_type,omitempty"`
	Amount     float64   `json:"amount"`
	Balance    float64   `json:"balance_at_the_moment"`
	Comment    string    `json:"information"`
}

type HistoryRequest struct {
	Id       int    `json:"id"`
	Limit    int    `json:"limit"`
	Offset   int    `json:"offset"`
	Type     string `json:"operation_type"`
	Order    string `json:"order"`
	SortType string `json:"sort_type"`
}

func dateToString(date time.Time) string {
	return date.Format("Jan 2, 2006 at 3:04pm")
}

func ExtractHistory(_db *sql.DB, res *[]HistoryFrame, req HistoryRequest) int {
	if req.Type == "all" {
		req.Type = ""
	}

	tmpl, _ := template.New("sqrReq").Parse(consts.HistoryTmpl)
	var query bytes.Buffer
	tmpl.Execute(&query, req)

	rows, _ := _db.Query(query.String())
	defer rows.Close()

	for rows.Next() {
		currentFrame := HistoryFrame{}
		rows.Scan(&currentFrame.Date, &currentFrame.Operation, &currentFrame.Amount, &currentFrame.Balance,
			&currentFrame.Comment)
		currentFrame.DateString = dateToString(currentFrame.Date)
		*res = append(*res, currentFrame)
	}

	return consts.OK
}
