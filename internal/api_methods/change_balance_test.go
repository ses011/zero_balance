package api_methods

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
	"zeroBalance/internal/consts"
)

func TestChangeBalanceValidate(t *testing.T) {
	t.Run("correct validate", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req := changeBalanceRequest{
			Id:            1,
			OperationType: "accrual",
			Amount:        321.12,
		}

		resp := changeBalanceValidate(_db, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
	})

	t.Run("unknown operation", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req := changeBalanceRequest{
			Id:            1,
			OperationType: "accrualadd",
			Amount:        321.12,
		}

		resp := changeBalanceValidate(_db, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.UnknownOperation, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.UnknownOperation], resp.Description)
	})

	t.Run("no user", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT")

		req := changeBalanceRequest{
			Id:            1,
			OperationType: "accrualadd",
			Amount:        321.12,
		}

		resp := changeBalanceValidate(_db, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.NoUser, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NoUser], resp.Description)
	})

	t.Run("negative amount", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req := changeBalanceRequest{
			Id:            1,
			OperationType: "accrual",
			Amount:        -321.12,
		}

		resp := changeBalanceValidate(_db, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.NegativeAmount, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NegativeAmount], resp.Description)
	})

	t.Run("zero amount", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req := changeBalanceRequest{
			Id:            1,
			OperationType: "accrual",
			Amount:        0,
		}

		resp := changeBalanceValidate(_db, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.NegativeAmount, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NegativeAmount], resp.Description)
	})
}
