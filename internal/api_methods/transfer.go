package api_methods

import (
	"database/sql"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"zeroBalance/internal/consts"
	"zeroBalance/internal/db"
)

type transferRequest struct {
	From   int     `json:"from"`
	To     int     `json:"to"`
	Amount float64 `json:"amount"`
}

type transferResponse struct {
	StatusCode  int    `json:"status_code"`
	Description string `json:"description"`
}

func transferValidate(_db *sql.DB, req transferRequest) transferResponse {
	var res transferResponse

	if req.To == req.From {
		res.StatusCode = consts.TransferToYourself
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}

	if !db.UserExists(_db, req.From) {
		res.StatusCode = consts.NoUserFrom
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}

	if !db.UserExists(_db, req.To) {
		res.StatusCode = consts.NoUserTo
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}

	if req.Amount <= 0 {
		res.StatusCode = consts.NegativeAmount
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}

	balance, err := db.GetBalance(_db, req.From)
	if err != nil || balance < req.Amount {
		res.StatusCode = consts.NotEnoughMoney
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}

	res.StatusCode = consts.OK
	res.Description = consts.Descriptions[consts.OK]

	return res
}

func Transfer(_db *sql.DB, ctx *gin.Context) {
	decoder := json.NewDecoder(ctx.Request.Body)
	var reqData transferRequest
	var respData transferResponse

	if err := decoder.Decode(&reqData); err == nil {
		respData = transferValidate(_db, reqData)
		if respData.StatusCode == consts.OK {
			errCode := db.Transfer(_db, reqData.From, reqData.To, reqData.Amount)
			if errCode == consts.OK {
				ctx.JSON(http.StatusOK, respData)
				return
			}

			respData.StatusCode = errCode
			respData.Description = consts.Descriptions[errCode]
		}
	} else {
		respData.StatusCode = consts.IncorrectJsonInput
		respData.Description = consts.Descriptions[consts.IncorrectJsonInput]
	}

	ctx.JSON(http.StatusBadRequest, respData)
}
