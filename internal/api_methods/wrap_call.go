package api_methods

import (
	"database/sql"
	"github.com/gin-gonic/gin"
)

func WrapCall(
	_db *sql.DB,
	a func(_db *sql.DB, ctx *gin.Context),
) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		a(_db, ctx)
	}
}
