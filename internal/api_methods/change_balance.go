package api_methods

import (
	"database/sql"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"zeroBalance/internal/consts"
	"zeroBalance/internal/db"
)

type changeBalanceRequest struct {
	Id            int     `json:"user_id"`
	OperationType string  `json:"operation_type"`
	Amount        float64 `json:"amount"`
}

type changeBalanceResponse struct {
	StatusCode  int     `json:"status_code"`
	Description string  `json:"description"`
	Id          int     `json:"user_id,omitempty"`
	Balance     float64 `json:"balance,omitempty"`
}

func changeBalanceValidate(_db *sql.DB, req changeBalanceRequest) changeBalanceResponse {
	var res changeBalanceResponse

	if !db.UserExists(_db, req.Id) {
		res.StatusCode = consts.NoUser
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}
	if req.OperationType != "accrual" && req.OperationType != "debiting" {
		res.StatusCode = consts.UnknownOperation
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}
	if req.Amount <= 0 {
		res.StatusCode = consts.NegativeAmount
		res.Description = consts.Descriptions[res.StatusCode]
		return res
	}

	res.StatusCode = consts.OK
	res.Description = consts.Descriptions[res.StatusCode]
	return res
}

func ChangeBalance(_db *sql.DB, ctx *gin.Context) {
	decoder := json.NewDecoder(ctx.Request.Body)
	var reqData changeBalanceRequest
	var respData changeBalanceResponse

	if err := decoder.Decode(&reqData); err == nil {
		respData = changeBalanceValidate(_db, reqData)
		if respData.StatusCode == consts.OK {
			balance, errCode := db.ChangeUserBalance(_db, reqData.Id, reqData.OperationType, reqData.Amount)
			if errCode == consts.OK {
				resp := changeBalanceResponse{StatusCode: consts.OK, Description: consts.Descriptions[consts.OK],
					Id: respData.Id, Balance: balance}
				ctx.JSON(http.StatusOK, resp)
				return
			}

			respData.StatusCode = errCode
			respData.Description = consts.Descriptions[errCode]
		}
	} else {
		respData.StatusCode = consts.IncorrectJsonInput
		respData.Description = consts.Descriptions[consts.IncorrectJsonInput]
	}

	ctx.JSON(http.StatusBadRequest, respData)
}
