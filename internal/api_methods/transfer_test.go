package api_methods

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
	"zeroBalance/internal/consts"
)

func TestTransferValidate(t *testing.T) {
	t.Run("correct validate", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"balance"}).
			AddRow(666.66)).RowsWillBeClosed()

		req := transferRequest{
			From:   1,
			To:     2,
			Amount: 123.33,
		}
		resp := transferValidate(_db, req)

		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
	})

	t.Run("no user from", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT")

		req := transferRequest{
			From:   1,
			To:     2,
			Amount: 123.33,
		}
		resp := transferValidate(_db, req)

		assert.Equal(t, consts.NoUserFrom, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NoUserFrom], resp.Description)
	})

	t.Run("no user to", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT")

		req := transferRequest{
			From:   1,
			To:     2,
			Amount: 123.33,
		}
		resp := transferValidate(_db, req)

		assert.Equal(t, consts.NoUserTo, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NoUserTo], resp.Description)
	})

	t.Run("negative amount", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req := transferRequest{
			From:   1,
			To:     2,
			Amount: -123.33,
		}
		resp := transferValidate(_db, req)

		assert.Equal(t, consts.NegativeAmount, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NegativeAmount], resp.Description)
	})

	t.Run("correct validate", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"balance"}).
			AddRow(12.66)).RowsWillBeClosed()

		req := transferRequest{
			From:   1,
			To:     2,
			Amount: 123.33,
		}
		resp := transferValidate(_db, req)

		assert.Equal(t, consts.NotEnoughMoney, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NotEnoughMoney], resp.Description)
	})
}
