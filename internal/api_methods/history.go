package api_methods

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"zeroBalance/internal/consts"
	"zeroBalance/internal/db"
)

type historyResponse struct {
	StatusCode  int               `json:"status_code"`
	Description string            `json:"description"`
	Initials    string            `json:"initials,omitempty"`
	Operations  []db.HistoryFrame `json:"operations"`
}

func historyValidate(_db *sql.DB, id, limit, offset int, _type, order, sortType string) (db.HistoryRequest,
	historyResponse) {
	var res historyResponse

	if limit == 0 {
		limit = 10
	}
	if _type == "" {
		_type = "all"
	}
	if order == "" {
		order = "desc"
	}
	if sortType == "" {
		sortType = "operation_date"
	}

	req := db.HistoryRequest{Id: id, Limit: limit, Offset: offset, Type: _type, Order: order, SortType: sortType}

	if !db.UserExists(_db, id) {
		res.StatusCode = consts.NoUser
		res.Description = consts.Descriptions[res.StatusCode]
		return req, res
	}

	if limit < 0 {
		res.StatusCode = consts.NegativeLimit
		res.Description = consts.Descriptions[res.StatusCode]
		return req, res
	}

	if offset < 0 {
		res.StatusCode = consts.NegativeOffset
		res.Description = consts.Descriptions[res.StatusCode]
		return req, res
	}

	if _type != "accrual" && _type != "debiting" && _type != "all" {
		res.StatusCode = consts.IncorrectTypeOfOperation
		res.Description = consts.Descriptions[res.StatusCode]
		return req, res
	}

	if order != "asc" && order != "desc" {
		res.StatusCode = consts.IncorrectOrder
		res.Description = consts.Descriptions[res.StatusCode]
		return req, res
	}

	if sortType != "operation_date" && sortType != "amount" {
		res.StatusCode = consts.IncorrectTypeOfSort
		res.Description = consts.Descriptions[res.StatusCode]
		return req, res
	}

	res.StatusCode = consts.OK
	res.Description = consts.Descriptions[consts.OK]

	return req, res
}

func parseHistory(_db *sql.DB, req *http.Request) (db.HistoryRequest, historyResponse) {
	id, _ := strconv.Atoi(req.URL.Query().Get("id"))
	limit, _ := strconv.Atoi(req.URL.Query().Get("limit"))
	offset, _ := strconv.Atoi(req.URL.Query().Get("offset"))
	_type := req.URL.Query().Get("type")
	order := req.URL.Query().Get("order")
	sortType := req.URL.Query().Get("sort_type")

	return historyValidate(_db, id, limit, offset, _type, order, sortType)
}

func History(_db *sql.DB, ctx *gin.Context) {
	reqData, respData := parseHistory(_db, ctx.Request)

	if respData.StatusCode == consts.OK {
		respData.Initials = db.GetInitialsById(_db, reqData.Id)
		db.ExtractHistory(_db, &respData.Operations, reqData)
		ctx.JSON(http.StatusOK, respData)
		return
	}

	ctx.JSON(http.StatusBadRequest, respData)
}
