package api_methods

import (
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"zeroBalance/internal/consts"
)

func TestExtractCurrencyRatio(t *testing.T) {
	t.Run("correct ration naming", func(t *testing.T) {
		rat, err := extractCurrencyRatio("USD")

		assert.Equal(t, consts.OK, err)
		assert.NotEqual(t, 0.0, rat)
	})

	t.Run("incorrect ration naming", func(t *testing.T) {
		rat, err := extractCurrencyRatio("JJJASD")

		assert.Equal(t, consts.NoSuchCurrency, err)
		assert.Equal(t, 0.0, rat)
	})
}

func TestGetBalanceValidate(t *testing.T) {
	t.Run("correct id and currency (RUB)", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		resp := getBalanceValidate(_db, 1, "RUB")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, 1, resp.Id)
		assert.Equal(t, "RUB", resp.Currency)
	})

	t.Run("correct id and currency (rub)", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		resp := getBalanceValidate(_db, 1, "rub")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, 1, resp.Id)
		assert.Equal(t, "RUB", resp.Currency)
	})

	t.Run("correct id and currency (USD)", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		resp := getBalanceValidate(_db, 1, "USD")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, 1, resp.Id)
		assert.Equal(t, "USD", resp.Currency)
	})

	t.Run("correct id and currency (usd)", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		resp := getBalanceValidate(_db, 1, "usd")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, 1, resp.Id)
		assert.Equal(t, "USD", resp.Currency)
	})

	t.Run("no user", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT")

		resp := getBalanceValidate(_db, 1, "USD")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.NoUser, resp.StatusCode)
	})

	t.Run("incorrect currency (JJJJ)", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		resp := getBalanceValidate(_db, 1, "JJJJ")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, consts.NoSuchCurrency, resp.StatusCode)
	})
}

func TestGetBalance(t *testing.T) {

	t.Run("get balance without currency correct", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"balance"}).
			AddRow(1123.32)).RowsWillBeClosed()

		GetBalance := WrapCall(_db, GetBalance)
		r := gin.Default()
		r.GET("/get_balance", GetBalance)
		writer := httptest.NewRecorder()

		url := "/get_balance?id=1"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := getBalanceResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
		assert.Equal(t, 1123.32, resp.Balance)
		assert.Equal(t, "RUB", resp.Currency)
	})

	t.Run("get balance with currency correct", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"balance"}).
			AddRow(1123.32)).RowsWillBeClosed()

		GetBalance := WrapCall(_db, GetBalance)
		r := gin.Default()
		r.GET("/get_balance", GetBalance)
		writer := httptest.NewRecorder()

		url := "/get_balance?id=1&currency=USD"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := getBalanceResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
		assert.NotEqual(t, 1123.32, resp.Balance)
		assert.Equal(t, "USD", resp.Currency)
	})

	t.Run("get balance no user", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT")

		GetBalance := WrapCall(_db, GetBalance)
		r := gin.Default()
		r.GET("/get_balance", GetBalance)
		writer := httptest.NewRecorder()

		url := "/get_balance?id=1&currency=USD"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := getBalanceResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.NoUser, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NoUser], resp.Description)
	})

	t.Run("get balance u", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		mock.ExpectQuery("SELECT")

		GetBalance := WrapCall(_db, GetBalance)
		r := gin.Default()
		r.GET("/get_balance", GetBalance)
		writer := httptest.NewRecorder()

		url := "/get_balance?id=1"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := getBalanceResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.NoBalance, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NoBalance], resp.Description)
	})

	t.Run("get balance with currency incorrect", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		GetBalance := WrapCall(_db, GetBalance)
		r := gin.Default()
		r.GET("/get_balance", GetBalance)
		writer := httptest.NewRecorder()

		url := "/get_balance?id=1&currency=JJJJ"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := getBalanceResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.NoSuchCurrency, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NoSuchCurrency], resp.Description)
	})
}
