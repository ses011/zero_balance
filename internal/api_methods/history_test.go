package api_methods

import (
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"zeroBalance/internal/consts"
)

func TestHistoryValidate(t *testing.T) {
	t.Run("correct validate", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req, resp := historyValidate(_db, 1, 10, 0, "accrual", "asc", "amount")

		assert.Equal(t, 1, req.Id)
		assert.Equal(t, 10, req.Limit)
		assert.Equal(t, 0, req.Offset)
		assert.Equal(t, "accrual", req.Type)
		assert.Equal(t, "asc", req.Order)
		assert.Equal(t, "amount", req.SortType)

		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
	})

	t.Run("no user", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT")

		req, resp := historyValidate(_db, 1, 10, 0, "accrual", "asc", "amount")

		assert.Equal(t, 1, req.Id)
		assert.Equal(t, 10, req.Limit)
		assert.Equal(t, 0, req.Offset)
		assert.Equal(t, "accrual", req.Type)
		assert.Equal(t, "asc", req.Order)
		assert.Equal(t, "amount", req.SortType)

		assert.Equal(t, consts.NoUser, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NoUser], resp.Description)
	})

	t.Run("negative limit", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req, resp := historyValidate(_db, 1, -10, 0, "accrual", "asc", "amount")

		assert.Equal(t, 1, req.Id)
		assert.Equal(t, -10, req.Limit)
		assert.Equal(t, 0, req.Offset)
		assert.Equal(t, "accrual", req.Type)
		assert.Equal(t, "asc", req.Order)
		assert.Equal(t, "amount", req.SortType)

		assert.Equal(t, consts.NegativeLimit, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NegativeLimit], resp.Description)
	})

	t.Run("negative offset", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req, resp := historyValidate(_db, 1, 10, -10, "accrual", "asc", "amount")

		assert.Equal(t, 1, req.Id)
		assert.Equal(t, 10, req.Limit)
		assert.Equal(t, -10, req.Offset)
		assert.Equal(t, "accrual", req.Type)
		assert.Equal(t, "asc", req.Order)
		assert.Equal(t, "amount", req.SortType)

		assert.Equal(t, consts.NegativeOffset, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NegativeOffset], resp.Description)
	})

	t.Run("incorrect type", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req, resp := historyValidate(_db, 1, 10, 10, "chinchopa", "asc", "amount")

		assert.Equal(t, 1, req.Id)
		assert.Equal(t, 10, req.Limit)
		assert.Equal(t, 10, req.Offset)
		assert.Equal(t, "chinchopa", req.Type)
		assert.Equal(t, "asc", req.Order)
		assert.Equal(t, "amount", req.SortType)

		assert.Equal(t, consts.IncorrectTypeOfOperation, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.IncorrectTypeOfOperation], resp.Description)
	})

	t.Run("incorrect order", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req, resp := historyValidate(_db, 1, 10, 10, "accrual", "dasc", "amount")

		assert.Equal(t, 1, req.Id)
		assert.Equal(t, 10, req.Limit)
		assert.Equal(t, 10, req.Offset)
		assert.Equal(t, "accrual", req.Type)
		assert.Equal(t, "dasc", req.Order)
		assert.Equal(t, "amount", req.SortType)

		assert.Equal(t, consts.IncorrectOrder, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.IncorrectOrder], resp.Description)
	})

	t.Run("incorrect sort_type", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		req, resp := historyValidate(_db, 1, 10, 10, "accrual", "desc", "amounut")

		assert.Equal(t, 1, req.Id)
		assert.Equal(t, 10, req.Limit)
		assert.Equal(t, 10, req.Offset)
		assert.Equal(t, "accrual", req.Type)
		assert.Equal(t, "desc", req.Order)
		assert.Equal(t, "amounut", req.SortType)

		assert.Equal(t, consts.IncorrectTypeOfSort, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.IncorrectTypeOfSort], resp.Description)
	})
}

func TestHistory(t *testing.T) {
	columns := []string{"operation_date", "operation_type", "amount", "current_balance", "comment"}

	t.Run("history correct", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"initials"}).
			AddRow("Egor")).RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).
				AddRow(time.Now(), "accrual", 123.32, 1233.21, "fff").
				AddRow(time.Now(), "debiting", 123.32, 1233.21, "fff")).
			RowsWillBeClosed()

		History := WrapCall(_db, History)
		r := gin.Default()
		r.GET("/history", History)
		writer := httptest.NewRecorder()

		url := "/history?id=1"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := historyResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
		assert.Equal(t, "Egor", resp.Initials)
		assert.Equal(t, 2, len(resp.Operations))

		assert.Equal(t, "accrual", resp.Operations[0].Operation)
		assert.Equal(t, 123.32, resp.Operations[0].Amount)
		assert.Equal(t, 1233.21, resp.Operations[0].Balance)
		assert.Equal(t, "fff", resp.Operations[0].Comment)

		assert.Equal(t, "debiting", resp.Operations[1].Operation)
		assert.Equal(t, 123.32, resp.Operations[1].Amount)
		assert.Equal(t, 1233.21, resp.Operations[1].Balance)
		assert.Equal(t, "fff", resp.Operations[1].Comment)
	})

	t.Run("history no operations", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()
		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"initials"}).
			AddRow("Egor")).RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).WillReturnRows(sqlmock.NewRows(columns))

		History := WrapCall(_db, History)
		r := gin.Default()
		r.GET("/history", History)
		writer := httptest.NewRecorder()

		url := "/history?id=1"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := historyResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
		assert.Equal(t, "Egor", resp.Initials)
		assert.Equal(t, 0, len(resp.Operations))
	})

	t.Run("negative limit", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery("SELECT").WillReturnRows(sqlmock.NewRows([]string{"count"}).
			AddRow(1)).RowsWillBeClosed()

		History := WrapCall(_db, History)
		r := gin.Default()
		r.GET("/history", History)
		writer := httptest.NewRecorder()

		url := "/history?id=1&limit=-12"

		req, _ := http.NewRequest("GET", url, nil)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := historyResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.NegativeLimit, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.NegativeLimit], resp.Description)
	})
}
