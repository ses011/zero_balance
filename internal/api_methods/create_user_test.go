package api_methods

import (
	"bytes"
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"zeroBalance/internal/consts"
)

func TestCreateUser(t *testing.T) {
	t.Run("create user correct", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		q := `INSERT INTO users (.+) returning uid`

		mock.ExpectQuery(q).
			WithArgs("foo").
			WillReturnRows(sqlmock.NewRows([]string{"uid"}).AddRow(1)).
			RowsWillBeClosed()

		r := gin.Default()
		CreateUser := WrapCall(_db, CreateUser)
		r.POST("/create_user", CreateUser)
		writer := httptest.NewRecorder()

		url := "/create_user"
		reqData := createUserRequest{Initials: "foo"}
		reader, _ := json.Marshal(reqData)

		req, _ := http.NewRequest("POST", url, bytes.NewReader(reader))
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := createUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.OK, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.OK], resp.Description)
		assert.Equal(t, 1, resp.Id)
	})

	t.Run("create user incorrect json", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		r := gin.Default()
		CreateUser := WrapCall(_db, CreateUser)
		r.POST("/create_user", CreateUser)
		writer := httptest.NewRecorder()

		url := "/create_user"
		reader := strings.NewReader("initials=asld;asd")

		req, _ := http.NewRequest("POST", url, reader)
		r.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := createUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.IncorrectJsonInput, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.IncorrectJsonInput], resp.Description)
	})

	t.Run("create user short initials", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		q := `INSERT INTO users (.+) returning uid`

		mock.ExpectQuery(q).
			WithArgs("foo").
			WillReturnRows(sqlmock.NewRows([]string{"uid"}).AddRow(1)).
			RowsWillBeClosed()

		r := gin.Default()
		CreateUser := WrapCall(_db, CreateUser)
		r.POST("/create_user", CreateUser)
		writer := httptest.NewRecorder()

		url := "/create_user"
		reqData := createUserRequest{}
		reader, _ := json.Marshal(reqData)

		req, _ := http.NewRequest("POST", url, bytes.NewReader(reader))
		r.ServeHTTP(writer, req)

		assert.Error(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := createUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.ShortInitials, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.ShortInitials], resp.Description)
	})

	t.Run("create user database error", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		q := `INSERT INTO users (.+) returning uid`

		mock.ExpectQuery(q).
			WithArgs("foo").
			WillReturnRows()

		r := gin.Default()
		CreateUser := WrapCall(_db, CreateUser)
		r.POST("/create_user", CreateUser)
		writer := httptest.NewRecorder()

		url := "/create_user"
		reqData := createUserRequest{Initials: "djjdj"}
		reader, _ := json.Marshal(reqData)

		req, _ := http.NewRequest("POST", url, bytes.NewReader(reader))
		r.ServeHTTP(writer, req)

		assert.Error(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := createUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, consts.DatabaseError, resp.StatusCode)
		assert.Equal(t, consts.Descriptions[consts.DatabaseError], resp.Description)
	})
}
