package api_methods

import (
	"database/sql"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"strings"
	"zeroBalance/internal/consts"
	"zeroBalance/internal/db"
)

type getBalanceResponse struct {
	StatusCode  int     `json:"status_code"`
	Description string  `json:"description,omitempty"`
	Balance     float64 `json:",omitempty"`
	Currency    string  `json:"currency,omitempty"`
	Ratio       float64 `json:"-"`
	Id          int     `json:"-"`
}

type Currencies struct {
	Rates map[string]float64 `json:"rates"`
}

func extractCurrencyRatio(currency string) (float64, int) {
	var currencies Currencies
	url := consts.CurrencyExchangeRateUrl

	resp, _ := http.Get(url)

	defer resp.Body.Close()
	json.NewDecoder(resp.Body).Decode(&currencies)

	rat, ok := currencies.Rates[strings.ToUpper(currency)]
	if !ok {
		return 0, consts.NoSuchCurrency
	}

	return rat, consts.OK
}

func getBalanceValidate(_db *sql.DB, id int, currency string) getBalanceResponse {
	var res getBalanceResponse

	if !db.UserExists(_db, id) {
		res.StatusCode = consts.NoUser
		res.Description = consts.Descriptions[consts.NoUser]
		return res
	}

	currency = strings.ToUpper(currency)

	if currency != "RUB" {
		res.Ratio, res.StatusCode = extractCurrencyRatio(currency)
	} else {
		res.StatusCode = consts.OK
		res.Ratio = 1
	}

	res.Description = consts.Descriptions[res.StatusCode]
	res.Id = id
	res.Currency = currency

	return res
}

func parseGetBalance(_db *sql.DB, req *http.Request) getBalanceResponse {
	id, _ := strconv.Atoi(req.URL.Query().Get("id"))
	currency := req.URL.Query().Get("currency")
	if currency == "" {
		currency = "RUB"
	}

	return getBalanceValidate(_db, id, currency)
}

func GetBalance(_db *sql.DB, ctx *gin.Context) {
	data := parseGetBalance(_db, ctx.Request)

	if data.StatusCode == consts.OK {
		balance, err := db.GetBalance(_db, data.Id)
		if err == nil {
			data.Balance = balance * data.Ratio
			ctx.JSON(http.StatusOK, data)
			return
		}

		data.StatusCode = consts.NoBalance
		data.Description = consts.Descriptions[consts.NoBalance]
	}

	data.Currency = ""

	ctx.JSON(http.StatusBadRequest, data)
}
