package api_methods

import (
	"database/sql"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"zeroBalance/internal/consts"
	"zeroBalance/internal/db"
)

type createUserResponse struct {
	Id          int    `json:"id,omitempty"`
	StatusCode  int    `json:"status_code"`
	Description string `json:"description"`
}

type createUserRequest struct {
	Initials string `json:"initials"`
}

func CreateUser(_db *sql.DB, ctx *gin.Context) {
	decoder := json.NewDecoder(ctx.Request.Body)
	var reqData createUserRequest
	var respData createUserResponse

	if err := decoder.Decode(&reqData); err == nil {
		id, code := db.CreateUser(_db, reqData.Initials)
		if code == consts.OK {
			respData.StatusCode = consts.OK
			respData.Description = consts.Descriptions[consts.OK]
			respData.Id = id

			ctx.JSON(http.StatusOK, respData)
			return
		} else {
			respData.StatusCode = code
			respData.Description = consts.Descriptions[code]
		}
	} else {
		respData.StatusCode = consts.IncorrectJsonInput
		respData.Description = consts.Descriptions[consts.IncorrectJsonInput]
	}

	ctx.JSON(http.StatusBadRequest, respData)
}
