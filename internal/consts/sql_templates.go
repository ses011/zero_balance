package consts

// consts for sql query templates
const (
	ChangeBalanceTmpl string = `
		BEGIN;
			WITH change_amount AS (
				UPDATE balances SET balance = balance {{.Op}} {{.Amount}} WHERE uid = {{.Uid}} RETURNING balance
			)

			INSERT INTO operations(uid, operation_type, amount, current_balance, comment)
			VALUES ({{.Uid}}, '{{.Operation}}', {{.Amount}}, (SELECT * FROM change_amount), '{{.Comment}}');
		COMMIT;`

	HistoryTmpl string = `
		BEGIN;
			SELECT operation_date, operation_type, amount, current_balance, comment 
			FROM operations WHERE uid = {{.Id}} AND operation_type ~ '^{{.Type}}' ORDER BY {{.SortType}}
			{{.Order}} LIMIT {{.Limit}} OFFSET {{.Offset}};
		COMMIT;`

	TransferTmpl string = `
			BEGIN;
			WITH debiting AS (
				UPDATE balances SET balance = balance - {{.Amount}} WHERE uid = {{.From}} RETURNING balance)

			INSERT INTO operations(uid, operation_type, amount, current_balance, comment) 
			VALUES ({{.From}}, 'debiting', {{.Amount}}, (SELECT * FROM debiting),
			'money was transferred to user {{.To}}({{.InitialsTo}})');

			WITH accrual AS (
				UPDATE balances SET balance = balance + {{.Amount}} WHERE uid = {{.To}} RETURNING balance)

			INSERT INTO operations(uid, operation_type, amount, current_balance, comment) 
			VALUES ({{.To}}, 'accrual', {{.Amount}}, (SELECT * FROM accrual),
			'money was received from user {{.From}}({{.InitialsFrom}})');
			COMMIT;`
)
