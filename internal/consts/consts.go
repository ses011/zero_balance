package consts

const (
	OK int = 0
)

// consts for operations table comments
const (
	Accrual  string = "replenishment of the balance from an external source"
	Debiting string = "withdrawal of funds to an external source"
)

// consts for GetBalance method
const (
	NoUser int = iota + 11
	NoBalance
	NoSuchCurrency
)

// consts for ChangeBalance method
const (
	IncorrectJsonInput int = iota + 21
	NegativeAmount
	UnknownOperation
	SmallBalance
)

// consts for History method
const (
	NegativeLimit int = iota + 31
	NegativeOffset
	IncorrectTypeOfOperation
	IncorrectOrder
	IncorrectTypeOfSort
)

// consts for Transfer method
const (
	NoUserFrom int = iota + 41
	NoUserTo
	NotEnoughMoney
	TransferToYourself
)

// consts for CreateUser method
const (
	ShortInitials int = 51 + iota
	DatabaseError
)

const CurrencyExchangeRateUrl string = "https://api.exchangerate.host/latest?base=RUB"

// Descriptions of errors map
var Descriptions = map[int]string{
	OK:                       "OK",
	NoUser:                   "No user with such id",
	NoBalance:                "This user hasnt balance yet",
	NoSuchCurrency:           "Unknown currency naming, please, use names from standard list of currency symbols",
	IncorrectJsonInput:       "Post requests contains json with invalid structure",
	NegativeAmount:           "Receive negative amount of money, should be positive",
	UnknownOperation:         "Operation should be 'accrual' or 'debiting'",
	SmallBalance:             "Not enough money in the account",
	NegativeLimit:            "Limit should be positive",
	NegativeOffset:           "Offset should be positive",
	IncorrectTypeOfOperation: "Operation type should be 'accrual' or 'debiting'",
	IncorrectOrder:           "Order should be 'asc' or 'desc'",
	IncorrectTypeOfSort:      "Sort type should be 'operation_date' or 'amount'",
	NoUserFrom:               "No user for transfer 'from' with such id",
	NoUserTo:                 "No user for transfer 'to' with such id",
	NotEnoughMoney:           "User for transfer 'from' hasnt enough money on balance",
	DatabaseError:            "Some troubles with database",
	ShortInitials:            "Incorrect initials len (should be more than 3 symbols)",
	TransferToYourself:       "You cant transfer money on yours balance",
}
