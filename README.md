### Инструкция по запуску:
1) Клонировать репозиторий.
2) Перейти в директорию deployments, а затем собрать и запустить проект командой `docker-compose up --build`.
3) По умолчанию в таблицу с пользователями добавляется один пользователь с `id=1`, добавить новых пользователей можно используя метод API CreateUser, который описан ниже;
### Методы API:
- GetBalance - позволяет получить баланс конкретного пользователя.
    - URL: http://localhost:8080/get_balance
    - Тип метода: `GET`
    - Обязательные URL параметры: `id=[integer]` (уникальный идентификатор пользователя)
    - Опциональные параметры: `currency=[string]` (наименование валюты, в которой должен быть получен баланс)
    - Ответ на удачный запрос: `{status_code: 0, description: "OK", balance: 113.32, currency: "RUB"}`
    - Ответ на неудачный запрос: `{status_code: 11, description: "No user with such id"}`
    - Пример использования: `curl "htpp://localhost:8080/get_balance?id=1&currency=USD"`
    - Коды ошибок и их описание: 11 (No user with such id) - пользователя с переданным уникальным идентификатором не существует в базе данных. 12 (This user hasnt balance yet) - пользователь зарегистрирован в системе, но на его баланс не поступало ни одного пополнения. 13 (Unknown currency naming) - неизвестное название валюты, допускаются только названия из <a href="https://www.xe.com/symbols.php"> списка наименования валют.</a> Данные о курсах валют взяты с сайта https://exchangerate.host/#/

- History - позволяет получить историю операций, производимых с балансом конкретного пользователя.
    - URL: http://localhost:8080/history
    - Тип метода: `GET`
    - Обязательные URL параметры: `id=[integer]` (уникальный идентификатор пользователя)
    - Опциональные параметры: `limit=[integer]` (максимальное число операций в ответе), `offset=[integer]` (смещение относительно первой операции), `type=[string]` (тип операции), `sort_type=[string]` (критерий, по которому проводится сортировка), `order=[string]` (порядок сортировки по выбранному критерию) 
    - Ответ на удачный запрос: `{status_code: 0, description: "OK", initials:"Egor", operations: [operation1, operation2, ...]}`, где `operation1` имеет формат `{date: "Aug 27, 2021 at 5:08pm", operation_type:"accrual", amount:500.00, balance_at_the_moment: 21349.99, information: "money was transferred from user 4(Dima)"}`
    - Ответ на неудачный запрос: `{status_code: 11, description: "No user with such id", operations: null}`
    - Пример использования: `curl "htpp://localhost:8080/history?id=1&limit=5&offset=2&sort_type=amount&order=desc`
    - Коды ошибок и их описание: 11 (No user with such id) - пользователя с переданным уникальным идентификатором не существует в базе данных. 31 (Limit should be positive) - параметр limit должен быть целым положительным числом, по умолчанию 10. 32 (Offset should be positive) - параметр offset должен быть целым неотрицательным числом, по умолчанию 0. 33 (Operation type should be 'accrual' or 'debiting') - параметр type должен принимать значения 'accrual' (начисление) или 'debiting' (списание), по умолчанию возвращаются операции и начисления, и списания. 34 (Order shoulb be asc or desc) - параметр order должен принимать значения 'asc' (сортировка по убыванию) или 'desc' (сортировка по возрастанию), по умолчанию 'desc'. 35 (Sort type should be 'operation_date' or 'amount') - параметр sort_type должен принимать значения 'operation_date' (сортировка по дате операций) или 'amount' (сортировка по количеству переведенных средств), по умолчанию 'operation_date'.

- ChangeBalance - позволяет изменить баланс определенного пользователя (либо зачислить средства с внешнего источника, либо вывести средства на внешний источник).
  - URL: http://localhost:8080/change_balance
  - Тип метода: `POST`
  - Поля входного json файла: `id=[integer]` (идентификатор пользователя), `operation_type=[string]` (тип операции), `amount=[float]` (сумма денег).
  - Ответ на удачный запрос: `{status_code: 0, description: "OK", balance: 113.32}`
  - Ответ на неудачный запрос: `{status_code: 11, description: "No user with such id"}`
  - Пример использования: `curl -X POST http://localhost:8080/change_balance -H 'Content-Type: application/json' -d '{"user_id":41,"operation_type":"accrual","amount":1123.33}'`
  - Коды ошибок и их описание: 11 (No user with such id) - пользователя с переданным уникальным идентификатором не существует в базе данных. 21 (Post request contains invalid json data) - структура входного json файла не соответсвует описанной ранее. 22 (Receive negative amount of money, should be positive) - параметр amount принимает отрицательное значение или 0. 23 (Unknown operation, should be 'accrual' or 'debiting') - параметр operation_type принимает значение, отличное от 'accrual' или 'debiting'. 24 (Not enough money on this account) - на балансе пользователя недостаточно средств для списания. 

- Transfer - позволяет перевести деньги с аккаунта одного пользователя на аккаунт другого.
  - URL: http://localhost:8080/transfer
  - Тип метода: `POST`
  - Поля входного json файла: `from=[integer]` (идентификатор пользователя, который переводит), `to=[integer]` (идентификатор пользователя, которому переводят), `amount=[float]` (сумма денег).
  - Ответ на удачный запрос: `{status_code: 0, description: "OK"}`
  - Ответ на неудачный запрос: `{status_code:42, description:"No user for transfer 'to' with such id"}`
  - Пример использования: `curl -X POST http://localhost:8080/transfer -H 'Content-Type: application/json' -d '{"from":1,"to":224,"amount":500.00}'`
  - Коды ошибок и их описание: 21 (Post request contains invalid json data) - структура входного json файла не соответсвует описанной ранее. 41 (No user for transfer 'from' with such id) - пользователя с идентификатором from не существует. 42 (No user for transfer 'to' with such id) - пользователя с идентификатором to не существует. 22 (Receive negative amount of money, should be positive) - параметр amount принимает отрицательное значение или 0. 43 (User for transfer 'from' hasnt enough money on balance) - у пользователя с индентификатором 'from' недостаточно средств на балансе. 44 (You cant transfer money on yours balance) - параметры to и from в переданном json файле совпадают, переводы со своего баланса самому себе не поддерживаются.

- CreateUser - позволяет добавить информацию о новом пользователе в базу данных.
  - URL: http://localhost:8080/transfer
  - Тип метода: `POST`
  - Поля входного json файла: `initials=[string]` (инициалы пользователя).
  - Ответ на удачный запрос: `{id: 123, status_code: 0, description: "OK"}`
  - Ответ на неудачный запрос: `{status_code:52, description:"Incorrect initials len (should be more than 3 symbols)"}`
  - Пример использования: `curl -X POST http://localhost:8080/create_user -H 'Content-Type: application/json' -d '{"initials":"Kekus"}'`
  - Коды ошибок и их описание: 51 (Incorrect initials len (should be more than 3 symbols)) - длина переданного параметра initials составляет 3 или менее символов, допускаются инициалы длиной 4 и более.